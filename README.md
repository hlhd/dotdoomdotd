# dotDoomdotD

install the `stow` package

install [doom and its dependencies](https://github.com/doomemacs/doomemacs/blob/master/docs/getting_started.org#install)

create `.emacs.d` directory in `$HOME`

create link of the cloned doom repo to `.emacs.d` directory, ex:
``` shell
$ stow -t ~/.emacs.d -v doom-repo-dir
```
where `doom-repo-dir` is where the cloned doom repo is stored

cd to `doom-repo-dir/bin`, then run:

``` shell
$ ./doom install
```

